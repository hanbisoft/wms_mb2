package com.coup.delv.data;

public class VehicleGetDownData {
    private boolean chk = false;
    private String alloccarNo;
    private String alloccarSeq;
    private String dockNm;
    private String courseNm;
    private String delvDegreeNm;
    private String itemNm;
    private String delv_degre;
    private String coures_cd;
    private String inlocCd;
    private String getdown_gbn = "";

    public String getGetdown_gbn() {
        return getdown_gbn;
    }

    public void setGetdown_gbn(String getdown_gbn) {
        this.getdown_gbn = getdown_gbn;
    }

    public String getInlocCd() {
        return inlocCd;
    }

    public void setInlocCd(String inlocCd) {
        this.inlocCd = inlocCd;
    }

    private int boxQty;
    private int eaQty;
    private int itemQty;


    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) {
        this.chk = chk;
    }

    public String getAlloccarNo() {
        return alloccarNo;
    }

    public void setAlloccarNo(String alloccarNo) {
        this.alloccarNo = alloccarNo;
    }

    public String getAlloccarSeq() {
        return alloccarSeq;
    }

    public void setAlloccarSeq(String alloccarSeq) {
        this.alloccarSeq = alloccarSeq;
    }

    public String getDockNm() {
        return dockNm;
    }

    public void setDockNm(String dockNm) {
        this.dockNm = dockNm;
    }

    public String getCourseNm() {
        return courseNm;
    }

    public void setCourseNm(String courseNm) {
        this.courseNm = courseNm;
    }

    public String getDelvDegreeNm() {
        return delvDegreeNm;
    }

    public void setDelvDegreeNm(String delvDegreeNm) {
        this.delvDegreeNm = delvDegreeNm;
    }

    public String getItemNm() {
        return itemNm;
    }

    public void setItemNm(String itemNm) {
        this.itemNm = itemNm;
    }

    public String getDelv_degre() {
        return delv_degre;
    }

    public void setDelv_degre(String delv_degre) {
        this.delv_degre = delv_degre;
    }

    public String getCoures_cd() {
        return coures_cd;
    }

    public void setCoures_cd(String coures_cd) {
        this.coures_cd = coures_cd;
    }

    public int getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(int boxQty) {
        this.boxQty = boxQty;
    }

    public int getEaQty() {
        return eaQty;
    }

    public void setEaQty(int eaQty) {
        this.eaQty = eaQty;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }
}
