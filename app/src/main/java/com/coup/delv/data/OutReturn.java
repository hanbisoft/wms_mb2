package com.coup.delv.data;

public class OutReturn {
    private boolean chk = false;
    private String custNm;
    private String addr;
    private String arrivalNm;
    private String item_nm;
    private int boxQty;
    private int eaQty;
    private String getdownGbnNm;

    public String getGetdownGbnNm() {
        return getdownGbnNm;
    }

    public void setGetdownGbnNm(String getdownGbnNm) {
        this.getdownGbnNm = getdownGbnNm;
    }

    private int itemQty;
    private String outReturnreqNo;
    private String outReturnreqSeq;

    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) {
        this.chk = chk;
    }

    public String getCustNm() {
        return custNm;
    }

    public void setCustNm(String custNm) {
        this.custNm = custNm;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getArrivalNm() {
        return arrivalNm;
    }

    public void setArrivalNm(String arrivalNm) {
        this.arrivalNm = arrivalNm;
    }

    public String getItem_nm() {
        return item_nm;
    }

    public void setItem_nm(String item_nm) {
        this.item_nm = item_nm;
    }

    public int getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(int boxQty) {
        this.boxQty = boxQty;
    }

    public int getEaQty() {
        return eaQty;
    }

    public void setEaQty(int eaQty) {
        this.eaQty = eaQty;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    public String getOutReturnreqNo() {
        return outReturnreqNo;
    }

    public void setOutReturnreqNo(String outReturnreqNo) {
        this.outReturnreqNo = outReturnreqNo;
    }

    public String getOutReturnreqSeq() {
        return outReturnreqSeq;
    }

    public void setOutReturnreqSeq(String outReturnreqSeq) {
        this.outReturnreqSeq = outReturnreqSeq;
    }


}
