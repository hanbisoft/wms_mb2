package com.coup.delv.data;

public class DeliverItemData {
    private boolean chk = false;
    private String alloccarNo;
    private String alloccarSeq;
    private String itemNm;
    private int boxQty;
    private int eaQty;
    private int itemQty;

    public boolean isChk() {
        return chk;
    }

    public void setChk(boolean chk) {
        this.chk = chk;
    }

    public String getAlloccarNo() {
        return alloccarNo;
    }

    public void setAlloccarNo(String alloccarNo) {
        this.alloccarNo = alloccarNo;
    }

    public String getAlloccarSeq() {
        return alloccarSeq;
    }

    public void setAlloccarSeq(String alloccarSeq) {
        this.alloccarSeq = alloccarSeq;
    }

    public String getItemNm() {
        return itemNm;
    }

    public void setItemNm(String itemNm) {
        this.itemNm = itemNm;
    }

    public int getBoxQty() {
        return boxQty;
    }

    public void setBoxQty(int boxQty) {
        this.boxQty = boxQty;
    }

    public int getEaQty() {
        return eaQty;
    }

    public void setEaQty(int eaQty) {
        this.eaQty = eaQty;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }
}
