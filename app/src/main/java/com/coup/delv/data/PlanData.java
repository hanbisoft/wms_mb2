package com.coup.delv.data;

public class PlanData {
    private String outDt;
    private String delvDegree;
    private String carCd;
    private String arrivalCd;
    private String delvTm;
    private String msgYn;
    private String custNm;
    private String arrivalNm;
    private String addr;
    private int delvSeq;
    private boolean backpos;

    public String getOutDt() {
        return outDt;
    }

    public void setOutDt(String outDt) {
        this.outDt = outDt;
    }

    public String getDelvDegree() {
        return delvDegree;
    }

    public void setDelvDegree(String delvDegree) {
        this.delvDegree = delvDegree;
    }

    public String getCarCd() {
        return carCd;
    }

    public void setCarCd(String carCd) {
        this.carCd = carCd;
    }

    public String getDelvTm() {
        return delvTm;
    }

    public void setDelvTm(String delvTm) {
        this.delvTm = delvTm;
    }

    public String getMsgYn() {
        return msgYn;
    }

    public void setMsgYn(String msgYn) {
        this.msgYn = msgYn;
    }

    public String getCustNm() {
        return custNm;
    }

    public void setCustNm(String custNm) {
        this.custNm = custNm;
    }

    public String getArrivalNm() {
        return arrivalNm;
    }

    public void setArrivalNm(String arrivalNm) {
        this.arrivalNm = arrivalNm;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getArrivalCd() {
        return arrivalCd;
    }

    public void setArrivalCd(String arrivalCd) {
        this.arrivalCd = arrivalCd;
    }

    public int getDelvSeq() {
        return delvSeq;
    }

    public void setDelvSeq(int delvSeq) {
        this.delvSeq = delvSeq;
    }

    public boolean isBackpos() {
        return backpos;
    }

    public void setBackpos(boolean backpos) {
        this.backpos = backpos;
    }
}
