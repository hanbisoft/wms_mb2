package com.coup.delv.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.delv.R;
import com.coup.delv.data.PlanData;

import java.util.ArrayList;

public class ListViewAdapterPlan extends BaseAdapter{
    LayoutInflater inflater = null;
    private Context context;
    private ArrayList<PlanData> m_oData = null;

    public ListViewAdapterPlan(Context context, ArrayList<PlanData> list) {
        this.context = context;
        m_oData = list;
    }

    @Override
    public int getCount() {
        return m_oData == null ? 0 : m_oData.size();
    }

    @Override
    public PlanData getItem(int position) {
        if(m_oData != null && m_oData.size() > 0) {
            return m_oData.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PlanData data = getItem(position);

        final ViewHolder vh;
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_plan, parent, false);
            vh = new ViewHolder();
            vh.backc = convertView.findViewById(R.id.backc);
            vh.tvCustNm = convertView.findViewById(R.id.tv_cust_nm);
            vh.tvArrivalNm = convertView.findViewById(R.id.tv_arrival_nm);
            vh.tvAddr = convertView.findViewById(R.id.tv_addr);
            vh.tvDelvTm = convertView.findViewById(R.id.tv_delv_tm);

            vh.data = data;
            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder)convertView.getTag();
        }

        vh.position = position;

        if(data.isBackpos())
            vh.backc.setBackgroundColor(Color.parseColor("#ffd400"));
        else if (!data.isBackpos() && position % 2 == 0)
            vh.backc.setBackgroundColor(Color.parseColor("#f1f1f1"));
        else
            vh.backc.setBackgroundColor(Color.parseColor("#ffffff"));

        vh.tvCustNm.setText(data.getCustNm());
        vh.tvArrivalNm.setText(data.getArrivalNm());
        vh.tvAddr.setText(data.getAddr());
        vh.tvDelvTm.setText(data.getDelvTm());

        return convertView;
    }



    private class ViewHolder
    {
        int position;
        LinearLayout backc;
        TextView tvCustNm;
        TextView tvArrivalNm;
        TextView tvAddr;
        TextView tvDelvTm;
        PlanData data;
    }
}