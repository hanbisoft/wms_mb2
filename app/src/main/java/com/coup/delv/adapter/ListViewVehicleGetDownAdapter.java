package com.coup.delv.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.coup.delv.R;
import com.coup.delv.data.VehicleGetDownData;
import java.util.ArrayList;
import com.coup.delv.data.ComCode;

public class ListViewVehicleGetDownAdapter extends BaseAdapter implements AdapterView.OnItemSelectedListener {
    LayoutInflater inflater = null;
    private Context context;
    private ArrayList<VehicleGetDownData> m_oData = null;
    private ArrayList<ComCode> getdownList;

    public ListViewVehicleGetDownAdapter(Context context, ArrayList<VehicleGetDownData> m_oData, ArrayList<ComCode> getdownList) {
        this.inflater = inflater;
        this.context = context;
        this.m_oData = m_oData;
        this.getdownList = getdownList;
    }

    @Override
    public int getCount() {
        return m_oData == null ? 0 : m_oData.size();
    }

    @Override
    public VehicleGetDownData getItem(int position) {
        if(m_oData != null && m_oData.size() > 0) {
            return m_oData.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        VehicleGetDownData data = getItem(position);

        final ListViewVehicleGetDownAdapter.ViewHolder vh;
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_vehicle_getdown, parent, false);
            vh = new ListViewVehicleGetDownAdapter.ViewHolder();
            vh.backc = convertView.findViewById(R.id.backc);
            vh.chk = convertView.findViewById(R.id.chk);
            vh.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int position = vh.position;
                    if(position > 0) {
                        m_oData.get(position).setChk(b);
                    }
                }
            });
            vh.chk.setTag(position);
            vh.tvItemNm = convertView.findViewById(R.id.tv_item_nm);
            vh.tvInlocCd = convertView.findViewById(R.id.tv_inloc_cd);
            vh.tvBoxEaQty = convertView.findViewById(R.id.tv_box_ea_qty);
            vh.etItemQty = convertView.findViewById(R.id.et_item_qty);

            vh.spGetdownGbn = convertView.findViewById(R.id.sp_getdown_gbn);
            vh.adGetdownGbn = new ListComCodeAdapter(context, data, getdownList);
            vh.spGetdownGbn.setAdapter(vh.adGetdownGbn);
            vh.spGetdownGbn.setOnItemSelectedListener(this);

            vh.etItemQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int position = vh.position;
                    if(position > 0) {
                        int itemQty = charSequence == null ? 0 : "".equals(charSequence.toString()) ? 0 : Integer.parseInt(charSequence.toString());
                        m_oData.get(position).setItemQty(itemQty);
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            vh.etItemQty.setTag(position);
            vh.data = data;
            convertView.setTag(vh);
        }
        else {
            vh = (ListViewVehicleGetDownAdapter.ViewHolder)convertView.getTag();
        }

        vh.position = position;

        vh.tvItemNm.setText(data.getItemNm());

        vh.chk.setChecked(data.isChk());

        int index = -1;
        for(int i=0; i<getdownList.size(); i++) {
            ComCode gbn = getdownList.get(i);
            if(data.getGetdown_gbn().equals(gbn.getCommCd())) {
                index = i;
                break;
            }
        }
        if(index > -1) {
            vh.spGetdownGbn.setSelection(index);
        }

        vh.etItemQty.setText(data.getItemQty() + "");
        vh.tvBoxEaQty.setText(data.getBoxQty() + " / " + data.getEaQty());

        vh.tvInlocCd.setText(data.getInlocCd());

        return convertView;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View convertView, int position, long l) {
        if(convertView != null) {
            VehicleGetDownData data = (VehicleGetDownData)convertView.getTag();
            data.setGetdown_gbn(getdownList.get(position).getCommCd());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }





    private class ViewHolder
    {
        int position;
        LinearLayout backc;
        CheckBox chk;
        TextView tvItemNm;
        TextView tvBoxEaQty;
        EditText etItemQty;
        TextView tvInlocCd;
        Spinner spGetdownGbn;
        ListComCodeAdapter adGetdownGbn;
        VehicleGetDownData data;
    }
}
