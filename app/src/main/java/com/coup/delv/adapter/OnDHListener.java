package com.sib.aselcni.delv.adapter;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by 신동현 on 2017-10-24.
 */

public interface OnDHListener {
    void onDHGetResult(String processer, SoapObject _result);
    void onDHSelectedListener(String code);
}