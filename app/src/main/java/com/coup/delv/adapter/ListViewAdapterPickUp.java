package com.coup.delv.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.delv.R;
import com.coup.delv.data.PickUpData;

import java.util.ArrayList;

public class ListViewAdapterPickUp extends BaseAdapter{
    LayoutInflater inflater = null;
    private Context context;
    private ArrayList<PickUpData> m_oData = null;

    public ListViewAdapterPickUp(Context context, ArrayList<PickUpData> list) {
        this.context = context;
        m_oData = list;
    }

    @Override
    public int getCount() {
        return m_oData == null ? 0 : m_oData.size();
    }

    @Override
    public PickUpData getItem(int position) {
        if(m_oData != null && m_oData.size() > 0) {
            return m_oData.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PickUpData data = getItem(position);

        final ViewHolder vh;
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_pickup, parent, false);
            vh = new ViewHolder();
            vh.backc = convertView.findViewById(R.id.backc);
            vh.chk = convertView.findViewById(R.id.chk);
            vh.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int position = vh.position;
                    if(position >= 0) {
                        m_oData.get(position).setChk(b);
                    }
                }
            });
            vh.chk.setTag(position);
            vh.tvItemNm = convertView.findViewById(R.id.tv_item_nm);
            vh.tvDockNm = convertView.findViewById(R.id.tv_dock_nm);
            vh.tvCourseNm = convertView.findViewById(R.id.tv_course_nm);
            vh.tvDelvDegreeNm = convertView.findViewById(R.id.tv_delv_degree_nm);
            vh.tvBoxEaQty = convertView.findViewById(R.id.tv_box_ea_qty);
            vh.etItemQty = convertView.findViewById(R.id.et_item_qty);
            vh.etItemQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int position = vh.position;
                    if(position >= 0) {
                        int itemQty = charSequence == null ? 0 : "".equals(charSequence.toString()) ? 0 : Integer.parseInt(charSequence.toString());
                        m_oData.get(position).setItemQty(itemQty);
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            vh.etItemQty.setTag(position);
            vh.data = data;
            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder)convertView.getTag();
        }

        vh.position = position;

        if(data.isScan()) {
            vh.backc.setBackgroundColor(Color.YELLOW);
        }
        else {
            if (position % 2 == 0)
                vh.backc.setBackgroundColor(Color.parseColor("#f1f1f1"));
            else
                vh.backc.setBackgroundColor(Color.parseColor("#ffffff"));
        }


        vh.tvItemNm.setText(data.getItemNm());
        vh.tvDockNm.setText(data.getDockNm());
        vh.tvCourseNm.setText(data.getCourseNm());
        vh.tvDelvDegreeNm.setText(data.getDelvDegreeNm());

        vh.chk.setChecked(data.isChk());

        vh.etItemQty.setText(data.getItemQty() + "");
        vh.tvBoxEaQty.setText(data.getBoxQty() + " / " + data.getEaQty());

        return convertView;
    }



    private class ViewHolder
    {
        int position;
        LinearLayout backc;
        CheckBox chk;
        TextView tvItemNm;
        TextView tvDockNm;
        TextView tvCourseNm;
        TextView tvDelvDegreeNm;
        TextView tvBoxEaQty;
        EditText etItemQty;
        PickUpData data;
    }
}