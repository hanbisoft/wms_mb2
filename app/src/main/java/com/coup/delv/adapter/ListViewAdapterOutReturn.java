package com.coup.delv.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coup.delv.R;
import com.coup.delv.data.ComCode;
import com.coup.delv.data.OutReturn;

import java.util.ArrayList;

public class ListViewAdapterOutReturn extends BaseAdapter implements AdapterView.OnItemSelectedListener {
    LayoutInflater inflater = null;
    private Context context;
    private ArrayList<OutReturn> m_oData = null;
    private ArrayList<ComCode> getdownList;

    public ListViewAdapterOutReturn(Context context, ArrayList<OutReturn> list) {
        this.context = context;
        m_oData = list;
    }

    @Override
    public int getCount() {
        return m_oData == null ? 0 : m_oData.size();
    }

    @Override
    public OutReturn getItem(int position) {
        if(m_oData != null && m_oData.size() > 0) {
            return m_oData.get(position);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        OutReturn data = getItem(position);

        final ListViewAdapterOutReturn.ViewHolder vh;
        if (convertView == null) {
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.row_inreturn, parent, false);
            vh = new ListViewAdapterOutReturn.ViewHolder();
            vh.backc = convertView.findViewById(R.id.backc);
            vh.chk = convertView.findViewById(R.id.chk);
            vh.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int position = vh.position;
                    if(position > 0) {
                        m_oData.get(position).setChk(b);
                    }
                }
            });
            vh.chk.setTag(position);
            vh.custNm = convertView.findViewById(R.id.tv_cust_nm);
            vh.addr = convertView.findViewById(R.id.tv_addr);
            vh.arrivalNm = convertView.findViewById(R.id.tv_arrival_nm);
            vh.item_nm = convertView.findViewById(R.id.tv_item_nm);
            vh.getdownGbnNm = convertView.findViewById(R.id.tv_getdown_gbn_nm);

            vh.tvBoxEaQty = convertView.findViewById(R.id.tv_box_ea_qty);
            vh.etItemQty = convertView.findViewById(R.id.et_item_qty);
            vh.etItemQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int position = vh.position;
                    if(position >= 0) {
                        int itemQty = charSequence == null ? 0 : "".equals(charSequence.toString()) ? 0 : Integer.parseInt(charSequence.toString());
                        m_oData.get(position).setItemQty(itemQty);
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            vh.etItemQty.setTag(position);

            vh.data = data;
            convertView.setTag(vh);
        }
        else {
            vh = (ListViewAdapterOutReturn.ViewHolder)convertView.getTag();
        }

        vh.position = position;

        vh.custNm.setText(data.getCustNm());
        vh.addr.setText(data.getAddr());
        vh.arrivalNm.setText(data.getArrivalNm());
        vh.item_nm.setText(data.getItem_nm());
        vh.getdownGbnNm.setText(data.getGetdownGbnNm());

        vh.chk.setChecked(data.isChk());

        int index = -1;

        vh.etItemQty.setText(data.getItemQty() + "");
        vh.tvBoxEaQty.setText(data.getBoxQty() + " / " + data.getEaQty());




        return convertView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }




    private class ViewHolder
    {
        int position;
        LinearLayout backc;
        CheckBox chk;
        TextView custNm;
        TextView addr;
        TextView arrivalNm;
        TextView item_nm;
        TextView getdownGbnNm;
        TextView tvBoxEaQty;
        EditText etItemQty;
        OutReturn data;
    }


}
