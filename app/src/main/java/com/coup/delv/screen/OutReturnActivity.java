package com.coup.delv.screen;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.delv.R;
import com.coup.delv.adapter.ListViewAdapterOutReturn;
import com.coup.delv.data.OutReturn;
import com.coup.delv.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import com.coup.delv.util.AselTran;

public class OutReturnActivity extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener {


    OutReturnActivity activity;

    CheckBox chkAll;
    TextView sDate;
    final Calendar cldr = Calendar.getInstance();
    DatePickerDialog picker;
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);
    int dayOfWeek = cldr.get(Calendar.DAY_OF_WEEK);
    String userid;
    String _param;
    public static Context CONTEXT;
    View ping;

    Button btnSave;
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    ListView listView;
    ListViewAdapterOutReturn adapter;
    ArrayList<OutReturn> list = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_return);
        activity = this;

        Intent intent = getIntent();
        sDate = findViewById(R.id.sdate);
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        tuserid.setText(userid + "님");
        CONTEXT = this;
        btnSave = findViewById(R.id.btn_save);
        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();
        ping = findViewById(R.id.ping);
        //  pingtest();
        btnSave.setOnClickListener(this);

        _param = userid;
        setDate();
        //onClick(null);
        //searchInboundGbn();
        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                Intent intent = new Intent(OutReturnActivity.this, loginActivity.class);
                                startActivity(intent);
                                onStop();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        listView = (ListView) findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new ListViewAdapterOutReturn(this, list);
        chkAll = findViewById(R.id.chk_all);
        chkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for(int i=0; i<list.size(); i++) {
                    list.get(i).setChk(b);
                }
                if(adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        //onClick(null);

    }

    private void searchList() {
        new SOAPLoadTask(this, this).execute("usp_MOB_OutReturn_list", SOAPLoadTask.convertParams(
                "LIST",
                sDate.getText().toString(),
                sDate.getText().toString()));
    }


    @Override
    public void onGetResult(String processer, SoapObject _result) {
        switch (processer) {
            case "usp_MOB_OutReturn_list" : {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        OutReturn data = new OutReturn();
                        data.setOutReturnreqNo(AselTran.GetValue(rs, "outreturnreq_no"));
                        data.setOutReturnreqSeq(AselTran.GetValue(rs, "outreturnreq_seq"));

                        data.setCustNm(AselTran.GetValue(rs, "cust_nm"));
                        data.setAddr(AselTran.GetValue(rs, "arrival_cd"));
                        data.setArrivalNm(AselTran.GetValue(rs, "arrival_nm"));
                        data.setItem_nm(AselTran.GetValue(rs, "item_nm"));
                        data.setGetdownGbnNm(AselTran.GetValue(rs, "outreturn_gbn_nm"));

                        data.setItemQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setBoxQty((int)Double.parseDouble(AselTran.GetValue(rs, "box_qty")));
                        data.setEaQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        list.add(data);
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                //adapter.notifyDataSetChanged();
                //listView.invalidateViews();
                //listView.refreshDrawableState();

                adapter = new ListViewAdapterOutReturn(this, list);
                listView.setAdapter(adapter);

                int list_amount = _result.getPropertyCount();
                Log.wtf("ik",String.valueOf(list_amount));
                if(list_amount==1){
                    Toast.makeText(this, "작업 리스트가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            }



        }

    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save: {
                int cnt = 0;
                for(int i=0; i<list.size(); i++) {
                    OutReturn data = list.get(i);
                    if(data.isChk()) {
                        cnt++;
                    }
                }
                if(cnt == 0) {
                    Toast.makeText(this, "저장할 작업이 없습니다.", Toast.LENGTH_SHORT).show();
                }
                else {
                    SaveTask task = new SaveTask();
                    task.execute();
                }
                break;
            }
        }
    }

    public void backbtn(View v) {
        onBackPressed();
    }


    public void setDate(){

        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new Date());
//        if(dayOfWeek==1)
//        cal.add(Calendar.DAY_OF_YEAR, -2);
//        else if(dayOfWeek==2)
//            cal.add(Calendar.DAY_OF_YEAR, -3);
//        else
//            cal.add(Calendar.DAY_OF_YEAR, -1);

        SimpleDateFormat fm = new SimpleDateFormat(
                "yyyy-MM-dd");
        String strDate = fm.format(cal.getTime());
        sDate.setText(strDate);

    }

    public void setDate(View v) {

        switch (v.getId()) {
            case R.id.sdate:
                picker = new DatePickerDialog(OutReturnActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                sDate.setText(year + "-" + String.format("%02d",monthOfYear + 1) + "-" + String.format("%02d",dayOfMonth));
                                //onClick(null);
                                //searchInboundGbn();A
                                searchList();
                            }
                        }, year, month, day);
                picker.show();
                break;

        }
    }



    public class SaveTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(OutReturnActivity.this);

        @Override
        protected void onPreExecute() {


            for(int i=0; i<list.size(); i++) {
                OutReturn data = list.get(i);
                if(data.isChk()) {
                    new SOAPLoadTask(activity, activity).execute("usp_MOB_OutReturn_i", SOAPLoadTask.convertParams("IN",userid,data.getOutReturnreqNo(),data.getOutReturnreqSeq(),String.valueOf(data.getItemQty())));
                }
            }

            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("저장 중입니다..");


            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
            ((OutReturnActivity) OutReturnActivity.CONTEXT).searchList();
        }
    }
}
