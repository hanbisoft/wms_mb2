package com.coup.delv.screen;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView;
import com.coup.delv.R;
import com.coup.delv.adapter.ComCodeAdapter;
import com.coup.delv.adapter.ListViewVehicleGetDownAdapter;
import com.coup.delv.data.ComCode;
import com.coup.delv.data.VehicleGetDownData;
import com.coup.delv.util.AselTran;
import com.coup.delv.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class VehicleGetDownActivity extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener, AdapterView.OnItemSelectedListener {

    VehicleGetDownActivity activity;

    String slipNo;
    public static Context CONTEXT;
    TextView sDate;
    final Calendar cldr = Calendar.getInstance();
    DatePickerDialog picker;
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);
    int dayOfWeek = cldr.get(Calendar.DAY_OF_WEEK);
    String userid;
    String _param;
    ArrayList<ComCode> getdownList = new ArrayList<>();

    ComCodeAdapter adCustCd;
    ArrayList<VehicleGetDownData> list = new ArrayList<>();
    View ping;
    Button btnSave;
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    ListView listView;
    ListViewVehicleGetDownAdapter adapter;

    Spinner spCarCd;
    Spinner spDelvDegree;

    ComCodeAdapter adCarCd;
    ArrayList<ComCode> listCarCd = new ArrayList<>();
    ComCodeAdapter adDelvDegree;
    ArrayList<ComCode> listDelvDegree = new ArrayList<>();


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_get_down);
        Intent intent = getIntent();
        sDate = findViewById(R.id.sdate);
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        tuserid.setText(userid + "님");
        CONTEXT = this;
        btnSave = findViewById(R.id.btn_save);
        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();
        ping = findViewById(R.id.ping);
        adCustCd = new ComCodeAdapter(this, getdownList);
        //  pingtest();
        btnSave.setOnClickListener(this);

        _param = userid;
        setDate();
        //onClick(null);
        //searchInboundGbn();
        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                Intent intent = new Intent(VehicleGetDownActivity.this, loginActivity.class);
                                startActivity(intent);
                                onStop();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        spCarCd = findViewById(R.id.sp_car_cd);
        adCarCd = new ComCodeAdapter(this, listCarCd);
        spCarCd.setAdapter(adCarCd);
        spCarCd.setOnItemSelectedListener(this);

        spDelvDegree = findViewById(R.id.sp_delv_degree);
        adDelvDegree = new ComCodeAdapter(this, listDelvDegree);
        spDelvDegree.setAdapter(adDelvDegree);
        spDelvDegree.setOnItemSelectedListener(this);

        listView = (ListView) findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new ListViewVehicleGetDownAdapter(this,list, getdownList);

        searchCarCd();
    }

    @Override
    public void onResume() {
        super.onResume();

        //onClick(null);

    }

    private void searchCarCd() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetCarCd", SOAPLoadTask.convertParams("LIST"));
    }

    private void searchDelvDegree() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetDelvDegree", SOAPLoadTask.convertParams("LIST"));
    }

    private void searchGetdownGbn() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetGetdownGbn", SOAPLoadTask.convertParams("LIST"));
    }


    private void searchList() {
        ComCode carCd = (ComCode)spCarCd.getSelectedItem();
        ComCode delvDegree = (ComCode)spDelvDegree.getSelectedItem();
        new SOAPLoadTask(this, this).execute("usp_MOB_VehicleGetDown", SOAPLoadTask.convertParams(
                "LIST", sDate.getText().toString(), sDate.getText().toString(),
                carCd != null ? carCd.getCommCd() : "",
                delvDegree != null ? delvDegree.getCommCd() : ""
        ));
    }

    private void getSlipNo() {
        new SOAPLoadTask(this, this).execute("usp_COM_GetSlipNo", SOAPLoadTask.convertParams("", "INP_PileUp", sDate.getText().toString().replace("-", ""), ""));
    }

    @Override
    public void onGetResult(String processer, SoapObject _result) {
        switch (processer) {
            case "usp_MOB_GetCarCd" : {
                listCarCd.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");
                        listCarCd.add( new ComCode(comm_cd, code_nm) );
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
//                code anh kim o day
                spCarCd = (Spinner)findViewById(R.id.sp_car_cd);
                adCarCd = new ComCodeAdapter(this, listCarCd);
                spCarCd.setAdapter(adCarCd);
                spCarCd.setOnItemSelectedListener(this);
                //adInboundGbn.notifyDataSetChanged();

                searchDelvDegree();
                break;
            }

            case "usp_MOB_GetDelvDegree" : {
                listDelvDegree.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");
                        listDelvDegree.add( new ComCode(comm_cd, code_nm) );
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                adDelvDegree = new ComCodeAdapter(this, listDelvDegree);
                spDelvDegree.setAdapter(adDelvDegree);
                spDelvDegree.setOnItemSelectedListener(this);
                //adCustCd.notifyDataSetChanged();

                searchGetdownGbn();

                break;
            }

            case "usp_MOB_GetGetdownGbn" : {
                getdownList.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");
                        getdownList.add( new ComCode(comm_cd, code_nm) );
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                searchList();


                break;
            }



            case "usp_MOB_VehicleGetDown" : {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        VehicleGetDownData data = new VehicleGetDownData();
                        data.setAlloccarNo(AselTran.GetValue(rs, "alloccar_no"));
                        data.setAlloccarSeq(AselTran.GetValue(rs, "alloccar_seq"));
                        data.setItemNm(AselTran.GetValue(rs, "item_nm"));
                        data.setItemQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setBoxQty((int)Double.parseDouble(AselTran.GetValue(rs, "box_qty")));
                        data.setEaQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setInlocCd(AselTran.GetValue(rs, "inloc_cd"));

                        list.add(data);
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                //adapter.notifyDataSetChanged();
                //listView.invalidateViews();
                //listView.refreshDrawableState();

                adapter = new ListViewVehicleGetDownAdapter(this, list, getdownList);
                listView.setAdapter(adapter);

                int list_amount = _result.getPropertyCount();
                Log.wtf("ik",String.valueOf(list_amount));
                if(list_amount==1){
                    Toast.makeText(this, "작업 리스트가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            }

            case "usp_COM_GetSlipNo" : {
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);
                        slipNo = AselTran.GetValue(rs, "slip_no");
                    }

                    VehicleGetDownActivity.SaveTask task = new VehicleGetDownActivity.SaveTask();
                    task.execute();

                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                break;
            }

        }

    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save: {
                int cnt = 0;
                for(int i=0; i<list.size(); i++) {
                    VehicleGetDownData data = list.get(i);
                    if(data.isChk()) {
                        cnt++;
                    }
                }
                if(cnt == 0) {
                    Toast.makeText(this, "저장할 작업이 없습니다.", Toast.LENGTH_SHORT).show();
                }
                else {
                    getSlipNo();
                }
                break;
            }
        }
    }

    public void backbtn(View v) {
        onBackPressed();
    }


    public void setDate(){

        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new Date());
//        if(dayOfWeek==1)
//        cal.add(Calendar.DAY_OF_YEAR, -2);
//        else if(dayOfWeek==2)
//            cal.add(Calendar.DAY_OF_YEAR, -3);
//        else
//            cal.add(Calendar.DAY_OF_YEAR, -1);

        SimpleDateFormat fm = new SimpleDateFormat(
                "yyyy-MM-dd");
        String strDate = fm.format(cal.getTime());
        sDate.setText(strDate);

    }

    public void setDate(View v) {

        switch (v.getId()) {
            case R.id.sdate:
                picker = new DatePickerDialog(VehicleGetDownActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                sDate.setText(year + "-" + String.format("%02d",monthOfYear + 1) + "-" + String.format("%02d",dayOfMonth));
                                //onClick(null);
                                //searchInboundGbn();A
                                searchList();
                            }
                        }, year, month, day);
                picker.show();
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        searchList();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public class SaveTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(VehicleGetDownActivity.this);

        @Override
        protected void onPreExecute() {


            for(int i=0; i<list.size(); i++) {
                VehicleGetDownData data = list.get(i);
                if(data.isChk()) {
                    new SOAPLoadTask(activity, activity).execute("usp_MOB_VehicleGetDown_i", SOAPLoadTask.convertParams("IN", userid, slipNo, data.getAlloccarNo(), data.getAlloccarSeq(), String.valueOf(data.getItemQty())));

                }
            }

            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("저장 중입니다..");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
            ((VehicleGetDownActivity) VehicleGetDownActivity.CONTEXT).searchList();
        }
    }
}
