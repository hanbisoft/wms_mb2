package com.coup.delv.screen;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.delv.R;
import com.coup.delv.adapter.ComCodeAdapter;
import com.coup.delv.adapter.ListViewAdapterPlan;
import com.coup.delv.data.ComCode;
import com.coup.delv.data.PlanData;
import com.coup.delv.util.AselTran;
import com.coup.delv.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by 신동현 on 2018-11-09.
 */
public class PlanActivity extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {

    PlanActivity activity;

    String slipNo;

    int startTime_p = 0, startHour_p, startMin_p, endMin_p, endTime_p = 0, endHour_p, betweenTime_p = 0, list_amount, pos = -1,debug_1=-1;
    SimpleDateFormat dateFormat = new SimpleDateFormat("HHmm");
    Date reqDate, curDate;
    String gettime;

    final Calendar cldr = Calendar.getInstance();
    DatePickerDialog picker;
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);
    int dayOfWeek = cldr.get(Calendar.DAY_OF_WEEK);
    String userid;
    String _param;
    public static Context CONTEXT;
    View ping;

    public static final String PREFS_NAME = "MyPrefsFile1";

    Button btnSave;
    SharedPreferences setting;
    SharedPreferences.Editor editor;

    Spinner spCarCd;
    Spinner spDelvDegree;

    ComCodeAdapter adCarCd;
    ArrayList<ComCode> listCarCd = new ArrayList<>();
    ComCodeAdapter adDelvDegree;
    ArrayList<ComCode> listDelvDegree = new ArrayList<>();

    ListView listView;
    ListViewAdapterPlan adapter;
    ArrayList<PlanData> list = new ArrayList<>();

    TextView nowtime;
    ImageButton seq_up;
    ImageButton seq_down;
    Spinner chooseEndTime;
    Spinner chooseStartTime;
    public CheckBox dontShowAgain;

    String out_dt, car_cd, delv_degree;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        activity = this;

        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        tuserid.setText(userid + "님");

        car_cd = intent.getStringExtra("car_cd");
        car_cd = car_cd == null ? "" : car_cd;
        delv_degree = intent.getStringExtra("delv_degree");
        delv_degree = delv_degree == null ? "" : delv_degree;

        CONTEXT = this;
        btnSave = findViewById(R.id.btn_save);

        seq_up = findViewById(R.id.seq_up);
        seq_down = findViewById(R.id.seq_down);
        nowtime = (TextView) findViewById(R.id.nowTime);
        nowtime.setOnClickListener(this);


        out_dt = intent.getStringExtra("out_dt");
        if(out_dt != null && out_dt != "") {
            nowtime.setText(out_dt);
        }
        else {
            setDate();
        }



        /*long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String getTime = sdf.format(date);
        nowtime.setText(getTime);*/

        spCarCd = findViewById(R.id.sp_car_cd);
        adCarCd = new ComCodeAdapter(this, listCarCd);
        spCarCd.setAdapter(adCarCd);
        spCarCd.setOnItemSelectedListener(this);

        spDelvDegree = findViewById(R.id.sp_delv_degree);
        adDelvDegree = new ComCodeAdapter(this, listDelvDegree);
        spDelvDegree.setAdapter(adDelvDegree);
        spDelvDegree.setOnItemSelectedListener(this);

        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();
        ping = findViewById(R.id.ping);
      //  pingtest();
        btnSave.setOnClickListener(this);

        _param = userid;
        //onClick(null);
        //searchInboundGbn();
        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                Intent intent = new Intent(PlanActivity.this, loginActivity.class);
                                startActivity(intent);
                               onStop();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        listView = (ListView) findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new ListViewAdapterPlan(this, list);
        listView.setOnItemClickListener(this);

        chooseEndTime = findViewById(R.id.end_time);
        chooseStartTime = findViewById(R.id.start_time);
        chooseStartTime.setSelection(8);
        chooseEndTime.setSelection(17);
        //얼럿다이얼로그
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        LayoutInflater adbInflater = LayoutInflater.from(this);
        View eulaLayout = adbInflater.inflate(R.layout.checkbox, null);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        final String skipMessage = settings.getString("skipMessage", "NOT checked");
        dontShowAgain = (CheckBox) eulaLayout.findViewById(R.id.skip);

        chooseStartTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                startHour_p = position + 1;
                startTime_p = 0;
                startMin_p = (position * 60) + 0;
                debug_1++;
                try {
                    time_cal();

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        chooseEndTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                endHour_p = position + 1;
                endTime_p = 0;
                endMin_p = (position * 60) + 0;
                debug_1++;
                try {
                    time_cal();

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        seq_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos == list.size() - 1 || pos == -1) return;
                list.get(pos).setBackpos(false);
                String tmA = list.get(pos).getDelvTm();
                String tmB = list.get(pos + 1).getDelvTm();
                list.get(pos).setDelvTm(tmB);
                list.get(pos + 1).setDelvTm(tmA);
                Collections.swap(list, pos, pos + 1);
                pos++;
                list.get(pos).setBackpos(true);
                adapter.notifyDataSetChanged();
                listView.smoothScrollToPosition (pos);
            }

        });

        seq_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos == 0 || pos == -1) return;
                list.get(pos).setBackpos(false);
                String tmA = list.get(pos).getDelvTm();
                String tmB = list.get(pos - 1).getDelvTm();
                list.get(pos).setDelvTm(tmB);
                list.get(pos - 1).setDelvTm(tmA);
                Collections.swap(list, pos, pos - 1);
                pos--;
                list.get(pos).setBackpos(true);
                //  m_oData.get(position).seq_no= m_oData.get(position).seq_no+1;
                adapter.notifyDataSetChanged();
                listView.smoothScrollToPosition (pos);
            }

        });

        searchCarCd();
    }

    public void time_cal() throws ParseException {

        if(list.size() == 0){
            return;
        }
        if(list.size() == 1) {
            list.get(0).setDelvTm(String.format("%02d:%02d", startHour_p, startTime_p));
            if(debug_1>=2){
                adapter.notifyDataSetChanged();
            }
            return;
        }

        int hap, temp;

        try {
            reqDate = dateFormat.parse(String.format("%02d%02d", startHour_p, startTime_p));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long reqDateTime = reqDate.getTime();

        try {
            curDate = dateFormat.parse(String.format("%02d%02d", endHour_p, endTime_p));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long curDateTime = curDate.getTime();

        long minute = (curDateTime - reqDateTime) / 60000;
        betweenTime_p = Math.abs((int) minute) / (list.size() - 1);
        first:
        for (int i = 0; i < list.size(); i++) {
//            if (Integer.valueOf(list.get(i).getDelvSeq()) > list.size()) {
//                break first;
//            }
            hap = startTime_p + betweenTime_p * i;
            temp = startHour_p;
            while (hap >= 60) {
                hap -= 60;
                temp++;
                if (temp > 24)
                    temp = 0;
            }

            gettime = String.format("%02d:%02d", temp, hap);
            list.get(i).setDelvTm(gettime);
        }

        if(debug_1>=2){
            adapter.notifyDataSetChanged();
        }
    }

    public void setDate(){

        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new Date());
//        if(dayOfWeek==1)
//        cal.add(Calendar.DAY_OF_YEAR, -2);
//        else if(dayOfWeek==2)
//            cal.add(Calendar.DAY_OF_YEAR, -3);
//        else
//            cal.add(Calendar.DAY_OF_YEAR, -1);

        SimpleDateFormat fm = new SimpleDateFormat(
                "yyyy-MM-dd");
        String strDate = fm.format(cal.getTime());
        nowtime.setText(strDate);

    }


    @Override
    public void onResume() {
        super.onResume();

       //onClick(null);

    }

    private void searchCarCd() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetCarCd", SOAPLoadTask.convertParams("LIST"));
    }


    private void searchDelvDegree() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetDelvDegree", SOAPLoadTask.convertParams("LIST"));
    }

    private void searchList() {
        ComCode carCd = (ComCode)spCarCd.getSelectedItem();
        ComCode delvDegree = (ComCode)spDelvDegree.getSelectedItem();

        new SOAPLoadTask(this, this).execute("usp_MOB_VehicleDeliverPlan_list", SOAPLoadTask.convertParams(
                "LIST",
                nowtime.getText().toString(),
                nowtime.getText().toString(),
                carCd != null ? carCd.getCommCd() : "",
                delvDegree != null ? delvDegree.getCommCd() : ""
        ));
    }

    @Override
    public void onGetResult(String processer, SoapObject _result) {
        switch (processer) {
            case "usp_MOB_GetCarCd" : {
                listCarCd.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");
                        if(!"".equals(comm_cd)) {
                            listCarCd.add(new ComCode(comm_cd, code_nm));
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                adCarCd = new ComCodeAdapter(this, listCarCd);
                spCarCd.setAdapter(adCarCd);
                spCarCd.setOnItemSelectedListener(null);
                int index = -1;
                for(int i=0; i<listCarCd.size(); i++) {
                    ComCode cd = listCarCd.get(i);
                    if(car_cd.equals(cd.getCommCd())) {
                        index = i;
                        break;
                    }
                }
                if(index > -1) {
                    spCarCd.setSelection(index);
                }
                spCarCd.setOnItemSelectedListener(this);
                //adInboundGbn.notifyDataSetChanged();

                searchDelvDegree();
                break;
            }

            case "usp_MOB_GetDelvDegree" : {
                listDelvDegree.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");

                        if(!"".equals(comm_cd)) {
                            listDelvDegree.add(new ComCode(comm_cd, code_nm));
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                adDelvDegree = new ComCodeAdapter(this, listDelvDegree);
                spDelvDegree.setAdapter(adDelvDegree);
                spDelvDegree.setOnItemSelectedListener(null);
                int index = -1;
                for(int i=0; i<listDelvDegree.size(); i++) {
                    ComCode cd = listDelvDegree.get(i);
                    if(delv_degree.equals(cd.getCommCd())) {
                        index = i;
                        break;
                    }
                }
                if(index > -1) {
                    spDelvDegree.setSelection(index);
                }
                spDelvDegree.setOnItemSelectedListener(this);
                //adCustCd.notifyDataSetChanged();

                searchList();

                break;
            }

            case "usp_MOB_VehicleDeliverPlan_list" : {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        PlanData data = new PlanData();
                        data.setOutDt(AselTran.GetValue(rs, "out_dt"));
                        data.setDelvDegree(AselTran.GetValue(rs, "delv_degree"));
                        data.setCarCd(AselTran.GetValue(rs, "car_cd"));
                        data.setArrivalCd(AselTran.GetValue(rs, "arrival_cd"));
                        data.setCustNm(AselTran.GetValue(rs, "cust_nm"));
                        data.setArrivalNm(AselTran.GetValue(rs, "arrival_nm"));
                        data.setAddr(AselTran.GetValue(rs, "addr"));
                        data.setDelvTm(AselTran.GetValue(rs, "delv_tm"));
                        data.setDelvSeq(i+1);
                        data.setMsgYn(AselTran.GetValue(rs, "msg_yn"));

                        list.add(data);
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                //adapter.notifyDataSetChanged();
                //listView.invalidateViews();
                //listView.refreshDrawableState();

                adapter = new ListViewAdapterPlan(this, list);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(this);

                try {
                    time_cal();

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int list_amount = _result.getPropertyCount();
                Log.wtf("ik",String.valueOf(list_amount));
                if(list_amount==1){
                    Toast.makeText(this, "작업 리스트가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            }


        }

    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save: {
                SaveTask task = new SaveTask();
                task.execute();
                break;
            }

            case R.id.nowTime: {
                picker = new DatePickerDialog(PlanActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                nowtime.setText(year + "-" + String.format("%02d", monthOfYear + 1) + "-" + String.format("%02d", dayOfMonth));
                                //onClick(null);
                                //searchInboundGbn();A
                                searchList();
                            }
                        }, year, month, day);
                picker.show();
                break;
            }
        }
    }

    public void backbtn(View v) {
        onBackPressed();
    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        searchList();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l_position) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setBackpos(false);
        }

        list.get(position).setBackpos(true);
        pos = position;
        adapter.notifyDataSetChanged();
    }


    public class SaveTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(PlanActivity.this);

        @Override
        protected void onPreExecute() {


            for(int i=0; i<list.size(); i++) {
                PlanData data = list.get(i);
                new SOAPLoadTask(activity, activity).execute("usp_MOB_VehicleDeliverPlan_iud", SOAPLoadTask.convertParams("IN", userid, data.getOutDt(), data.getCarCd(), data.getDelvDegree(), data.getArrivalCd(), String.valueOf(i + 1), data.getDelvTm()));
            }

            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("저장 중입니다..");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
            ((PlanActivity) PlanActivity.CONTEXT).searchList();
        }
    }
}
