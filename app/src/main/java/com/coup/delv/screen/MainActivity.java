package com.coup.delv.screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.coup.delv.R;


public class MainActivity extends AppCompatActivity {

    SharedPreferences setting;
    SharedPreferences.Editor editor;
    boolean update_bol;
    View ping;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent intent = getIntent();
        final String userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        tuserid.setText(userid + "님");


        ping = findViewById(R.id.ping);
        //   pingtest();
        /*new Thread()
        {
            public void run()
            {
                try {

                    StringBuffer sb = new StringBuffer();
                    PackageManager pm = getPackageManager();
                    PackageInfo packageInfo;
                    packageInfo = pm.getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
                    //String version = packageInfo.versionName; //버전 네임
                    int cVer = packageInfo.versionCode; //버전 코드

                    String page = "http://mall.ppang.biz/COM/version.txt";
                    URL url = new URL(page);

                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(url.openStream()));

                    String str = null;
                    while ((str = reader.readLine()) != null) {
                        sb.append(str);
                    }

                    //int cVer = Util.ToInt(curVer);
                    int nVer = Integer.parseInt(sb.toString());

                    if (cVer<nVer) {
                        update_bol=true;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                MainActivity.this.runOnUiThread(new Runnable()
                {
                    public void run() {
                        if(update_bol){
                            AlertDialog.Builder db = new AlertDialog.Builder(MainActivity.this);
                        db.setTitle(R.string.app_name)
                                .setMessage("프로그램이 업데이트 되었습니다\n다운로드 페이지로 이동하시겠습니까?")
                                .setCancelable(true)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // 사이트 열고
                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                        Uri u = Uri.parse("http://mall.ppang.biz");
                                        i.setData(u);
                                        startActivity(i);

                                        // 종료
                                        System.exit(0);
                                    }
                                })
                                .setNegativeButton(android.R.string.cancel, null)
                                .show();
                    }}
                });
            }
        }.start();*/

        //redirect to page

//        ViewGroup deli = findViewById(R.id.inspect_List);
//        ViewGroup order = findViewById(R.id.pileUp_List);
//        ViewGroup tack_back = findViewById(R.id.inReturn_List);


        ViewGroup GetDown = findViewById(R.id.activity_getdown);

        GetDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, VehicleGetDownActivity.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });

        ViewGroup pickup = findViewById(R.id.activity_pickup);
        pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, PickUpActivity.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });

        ViewGroup plan = findViewById(R.id.activity_plan);
        plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, PlanActivity.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });

        ViewGroup deliver = findViewById(R.id.activity_deliver);
        deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, DeliverActivity.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });

        ViewGroup inReturn = findViewById(R.id.activity_return);
        inReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, OutReturnActivity.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });

        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();


        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.change_pw:
                                Intent intent = new Intent(MainActivity.this, pw_change.class);
                                intent.putExtra("userid", userid);
                                startActivity(intent);
                                break;
                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                intent = new Intent(MainActivity.this, loginActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

    }

    public void backbtn(View v) {
        onBackPressed();
    }

    public void pingtest() {
        String host = "mall.ppang.biz";
        String cmd = "ping -c 1 -W 10 " + host;
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            proc.waitFor();
            int result = proc.exitValue();
            if (result == 2)
                ping.setBackgroundColor(Color.parseColor("#FF3636"));
            else if (result == 1)
                ping.setBackgroundColor(Color.parseColor("#FFBB00"));
            Log.e("ping", String.valueOf(result));

        } catch (Exception e) {
            Log.e("ping", e.getMessage());
        }
    }


}