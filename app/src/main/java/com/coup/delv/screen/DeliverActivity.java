package com.coup.delv.screen;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.delv.R;
import com.coup.delv.adapter.ComCodeAdapter;
import com.coup.delv.adapter.ListViewAdapterDeliver;
import com.coup.delv.data.ComCode;
import com.coup.delv.data.DeliverData;
import com.coup.delv.util.AselTran;
import com.coup.delv.util.SOAPLoadTask;

import org.ksoap2.serialization.SoapObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by 신동현 on 2018-11-09.
 */
public class DeliverActivity extends AppCompatActivity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {

    DeliverActivity activity;

    String slipNo;

    TextView sDate;
    final Calendar cldr = Calendar.getInstance();
    DatePickerDialog picker;
    int day = cldr.get(Calendar.DAY_OF_MONTH);
    int month = cldr.get(Calendar.MONTH);
    int year = cldr.get(Calendar.YEAR);
    int dayOfWeek = cldr.get(Calendar.DAY_OF_WEEK);
    String userid;
    String _param;
    public static Context CONTEXT;
    View ping;

    Button btnPlan;
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    Spinner spCarCd;
    Spinner spDelvDegree;

    ComCodeAdapter adCarCd;
    ArrayList<ComCode> listCarCd = new ArrayList<>();
    ComCodeAdapter adDelvDegree;
    ArrayList<ComCode> listDelvDegree = new ArrayList<>();

    ListView listView;
    ListViewAdapterDeliver adapter;
    ArrayList<DeliverData> list = new ArrayList<>();


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver);
        activity = this;

        Intent intent = getIntent();
        sDate = findViewById(R.id.sdate);
        userid = intent.getStringExtra("userid");
        TextView tuserid = findViewById(R.id.userid);
        tuserid.setText(userid + "님");
        CONTEXT = this;
        btnPlan = findViewById(R.id.btn_plan);

        spCarCd = findViewById(R.id.sp_car_cd);
        adCarCd = new ComCodeAdapter(this, listCarCd);
        spCarCd.setAdapter(adCarCd);
        spCarCd.setOnItemSelectedListener(this);

        spDelvDegree = findViewById(R.id.sp_delv_degree);
        adDelvDegree = new ComCodeAdapter(this, listDelvDegree);
        spDelvDegree.setAdapter(adDelvDegree);
        spDelvDegree.setOnItemSelectedListener(this);

        setting = getSharedPreferences("setting", 0);
        editor = setting.edit();
        ping = findViewById(R.id.ping);
      //  pingtest();
        btnPlan.setOnClickListener(this);

        _param = userid;
        setDate();
        //onClick(null);
        //searchInboundGbn();
        findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.logout:
                                editor.clear();
                                editor.commit();
                                Intent intent = new Intent(DeliverActivity.this, loginActivity.class);
                                startActivity(intent);
                               onStop();
                                break;
                        }
                        return false;
                    }
                });// to implement on click event on items of menu
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_main, popup.getMenu());
                popup.show();
            }
        });

        listView = (ListView) findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new ListViewAdapterDeliver(this, list);
        listView.setOnItemClickListener(this);


        searchCarCd();
    }

    @Override
    public void onResume() {
        super.onResume();

       //onClick(null);

    }

    private void searchCarCd() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetCarCd", SOAPLoadTask.convertParams("LIST"));
    }

    private void searchDelvDegree() {
        new SOAPLoadTask(this, this).execute("usp_MOB_GetDelvDegree", SOAPLoadTask.convertParams("LIST"));
    }

    private void searchList() {
        ComCode carCd = (ComCode)spCarCd.getSelectedItem();
        ComCode delvDegree = (ComCode)spDelvDegree.getSelectedItem();

        new SOAPLoadTask(this, this).execute("usp_MOB_VehicleDeliver_list", SOAPLoadTask.convertParams(
                "LIST",
                sDate.getText().toString(),
                sDate.getText().toString(),
                carCd != null ? carCd.getCommCd() : "",
                delvDegree != null ? delvDegree.getCommCd() : ""
        ));
    }

    @Override
    public void onGetResult(String processer, SoapObject _result) {
        switch (processer) {
            case "usp_MOB_GetCarCd" : {
                listCarCd.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");
                        if(!"".equals(comm_cd)) {
                            listCarCd.add(new ComCode(comm_cd, code_nm));
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                adCarCd = new ComCodeAdapter(this, listCarCd);
                spCarCd.setAdapter(adCarCd);
                spCarCd.setOnItemSelectedListener(this);
                //adInboundGbn.notifyDataSetChanged();

                searchDelvDegree();
                break;
            }

            case "usp_MOB_GetDelvDegree" : {
                listDelvDegree.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        String comm_cd = AselTran.GetValue(rs, "comm_cd");
                        String code_nm = AselTran.GetValue(rs, "code_nm");
                        if(!"".equals(comm_cd)) {
                            listDelvDegree.add(new ComCode(comm_cd, code_nm));
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                adDelvDegree = new ComCodeAdapter(this, listDelvDegree);
                spDelvDegree.setAdapter(adDelvDegree);
                spDelvDegree.setOnItemSelectedListener(this);
                //adCustCd.notifyDataSetChanged();

                searchList();

                break;
            }

            case "usp_MOB_VehicleDeliver_list" : {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        DeliverData data = new DeliverData();
                        data.setOutDt(AselTran.GetValue(rs, "out_dt"));
                        data.setDelvDegree(AselTran.GetValue(rs, "delv_degree"));
                        data.setCarCd(AselTran.GetValue(rs, "car_cd"));
                        data.setArrivalCd(AselTran.GetValue(rs, "arrival_cd"));
                        data.setCustNm(AselTran.GetValue(rs, "cust_nm"));
                        data.setArrivalNm(AselTran.GetValue(rs, "arrival_nm"));
                        data.setAddr(AselTran.GetValue(rs, "addr"));
                        data.setDelvTm(AselTran.GetValue(rs, "delv_tm"));
                        data.setDelvSeq((int)Double.parseDouble(AselTran.GetValue(rs, "delv_seq")));
                        data.setMsgYn(AselTran.GetValue(rs, "msg_yn"));

                        list.add(data);
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                //adapter.notifyDataSetChanged();
                //listView.invalidateViews();
                //listView.refreshDrawableState();

                adapter = new ListViewAdapterDeliver(this, list);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(this);

                int list_amount = _result.getPropertyCount();
                Log.wtf("ik",String.valueOf(list_amount));
                if(list_amount==1){
                    Toast.makeText(this, "작업 리스트가 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }

    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            searchList();
        }
        else if (requestCode == 2) {
            searchList();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_plan: {
                ComCode carCd = (ComCode)spCarCd.getSelectedItem();
                ComCode delvDegree = (ComCode)spDelvDegree.getSelectedItem();

                Intent intent = new Intent(DeliverActivity.this, PlanActivity.class);
                intent.putExtra("userid", userid);
                intent.putExtra("out_dt", sDate.getText().toString());
                intent.putExtra("car_cd", carCd == null ? "" : carCd.getCommCd());
                intent.putExtra("delv_degree", delvDegree == null ? "" : delvDegree.getCommCd());
                startActivityForResult(intent, 1);
                break;
            }
        }
    }

    public void backbtn(View v) {
        onBackPressed();
    }


    public void setDate(){

        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new Date());
//        if(dayOfWeek==1)
//        cal.add(Calendar.DAY_OF_YEAR, -2);
//        else if(dayOfWeek==2)
//            cal.add(Calendar.DAY_OF_YEAR, -3);
//        else
//            cal.add(Calendar.DAY_OF_YEAR, -1);

        SimpleDateFormat fm = new SimpleDateFormat(
                "yyyy-MM-dd");
        String strDate = fm.format(cal.getTime());
        sDate.setText(strDate);

    }

    public void setDate(View v) {

        switch (v.getId()) {
            case R.id.sdate:
                picker = new DatePickerDialog(DeliverActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                sDate.setText(year + "-" + String.format("%02d",monthOfYear + 1) + "-" + String.format("%02d",dayOfMonth));
                                //onClick(null);
                                //searchInboundGbn();A
                                searchList();
                            }
                        }, year, month, day);
                picker.show();
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        searchList();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        DeliverData data = list.get(i);
        Intent intent = new Intent(DeliverActivity.this, PopupDeliver.class);
        intent.putExtra("userid", userid);
        intent.putExtra("out_dt", data.getOutDt());
        intent.putExtra("car_cd", data.getCarCd());
        intent.putExtra("delv_degree", data.getDelvDegree());
        intent.putExtra("arrival_cd", data.getArrivalCd());
        intent.putExtra("cust_nm", data.getCustNm());
        intent.putExtra("arrival_nm", data.getArrivalNm());
        intent.putExtra("addr", data.getAddr());
        startActivityForResult(intent, 2);
    }
}
