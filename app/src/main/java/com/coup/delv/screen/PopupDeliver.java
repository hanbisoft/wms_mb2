package com.coup.delv.screen;

/**
 * Created by 신동현 on 2018-11-12.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.coup.delv.R;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.coup.delv.adapter.ListViewAdapterDeliverItem;
import com.coup.delv.data.DeliverItemData;
import com.coup.delv.util.AselTran;
import com.coup.delv.util.COM;
import com.coup.delv.util.SOAPLoadTask;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PopupDeliver extends Activity implements SOAPLoadTask.OnResultListener, SOAPLoadTask.OnErrorListener, View.OnClickListener {

    String slipNo;
    SignaturePad signaturePad;
    Button saveButton, clearButton;
    CheckBox chkAll;
    TextView torder_nm, tcust_nm, tarrival_nm, taddr, titem_nm, torder_qty, tprice_amt, nowtime, tvCustNm, tvArrivalNm, tvAddr;
    String out_dt, car_cd, delv_degree, arrival_cd;
    int pos, list_amount, endsu;
    List<String> _param2 = null;
    ListView m_oListView;
    String order_seq;
    Bitmap signImage = null;
    String userid,ocheck;

    public static Context CONTEXT;
    ListViewAdapterDeliverItem adapter;
    ArrayList<DeliverItemData> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_deliver_item);
        CONTEXT = this;
        signaturePad = (SignaturePad) findViewById(R.id.signaturePad);
        saveButton = (Button) findViewById(R.id.saveButton);
        clearButton = (Button) findViewById(R.id.clearButton);
        nowtime = (TextView) findViewById(R.id.nowTime);
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd  hh:mm:ss");

        String getTime = sdf.format(date);
        nowtime.setText(getTime);
        //disable both buttons at start
        saveButton.setEnabled(false);
        clearButton.setEnabled(false);

        tvCustNm = findViewById(R.id.tv_cust_nm);
        tvArrivalNm = findViewById(R.id.tv_arrival_nm);
        tvAddr = findViewById(R.id.tv_addr);

        Intent intent = getIntent();
        userid = intent.getStringExtra("userid");
        out_dt = intent.getStringExtra("out_dt");
        car_cd = intent.getStringExtra("car_cd");
        delv_degree = intent.getStringExtra("delv_degree");
        arrival_cd = intent.getStringExtra("arrival_cd");

        tvCustNm.setText(intent.getStringExtra("cust_nm"));
        tvArrivalNm.setText(intent.getStringExtra("arrival_nm"));
        tvAddr.setText(intent.getStringExtra("addr"));

        pos = intent.getIntExtra("position", 0);

        m_oListView = (ListView) findViewById(R.id.listitem);

        chkAll = findViewById(R.id.chk_all);
        chkAll.setChecked(true);
        chkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for(int i=0; i<list.size(); i++) {
                    list.get(i).setChk(b);
                }
                if(adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                saveButton.setEnabled(true);
                clearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                saveButton.setEnabled(false);
                clearButton.setEnabled(false);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int cnt = 0;
                for(int i=0; i<list.size(); i++) {
                    DeliverItemData data = list.get(i);
                    if(data.isChk()) {
                        cnt++;
                    }
                }
                if(cnt == 0) {
                    Toast.makeText(CONTEXT, "납품할 상품을 선택해 주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }

                getSlipNo();


            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
            }
        });

        searchList();
    }




    //메인 쓰레드 X
   public void SaveAndDialog(final String encodedImage)
    {

        for (int i = 0; i < list.size(); i++) {
            DeliverItemData data = list.get(i);
            new SOAPLoadTask((SOAPLoadTask.OnResultListener) this, this).execute("usp_MOB_VehicleDeliver_iud", SOAPLoadTask.convertParams("IN", userid, slipNo, data.getAlloccarNo(), data.getAlloccarSeq(), String.valueOf(data.getItemQty())));
        }

        new Thread(new Runnable() {
            public void run()
            {
                // 저장 시작.

                SaveSign(encodedImage);

            }
        }).start();
    }


    boolean SaveSign(String encodedImage) {
        String rec_msg = "";
        try {
            SoapObject request = new SoapObject(COM.NAMESPACE, "AndroidDeliverySignSave" + "");

            //string comp_cd, string slip_no, string svc_grd, string strImgByte
            request.addProperty("comp_cd", "10");
            request.addProperty("slip_no", slipNo);    // 주문번호
            request.addProperty("svc_grd", "10");
            request.addProperty("strImgByte",encodedImage);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE adt = new HttpTransportSE(COM.URLSRV);
            //adt.call(COM.SOAP_ACTION + COM.SAVE_METHOD, envelope);
            adt.call("http://tempuri.org/AndroidDeliverySignSave", envelope);

            Object ob = envelope.getResponse();
            rec_msg = ob.toString();

            String[] param = rec_msg.split("\\^");    //. | ^ 등 특수한경우에는 이스케이프한다.
            String workFlag = param[0];
            String Msg = param[1];


            if (!workFlag.equals("T")) {
                return false;
            }

            finish();

            return true;
        } catch (Exception err) {
            err.getStackTrace();
            err.printStackTrace();
            return false;
        }
    }


    public void searchList() {
        new SOAPLoadTask(this, this).execute("usp_MOB_VehicleDeliver_list", SOAPLoadTask.convertParams(
                "ITEM",
                out_dt,
                out_dt,
                car_cd,
                delv_degree,
                arrival_cd
        ));
    }

    private void getSlipNo() {
        new SOAPLoadTask(this, this).execute("usp_COM_GetSlipNo", SOAPLoadTask.convertParams("", "DLV_VehicleDeliver", out_dt.replace("-", ""), ""));
    }

    @Override
    public void onGetResult(String processer, SoapObject _result) {
        switch (processer) {
            case "usp_MOB_VehicleDeliver_list" : {
                list.clear();
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);

                        DeliverItemData data = new DeliverItemData();
                        data.setChk(true);
                        data.setAlloccarNo(AselTran.GetValue(rs, "alloccar_no"));
                        data.setAlloccarSeq(AselTran.GetValue(rs, "alloccar_seq"));
                        data.setItemNm(AselTran.GetValue(rs, "item_nm"));
                        data.setItemQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        data.setBoxQty((int)Double.parseDouble(AselTran.GetValue(rs, "box_qty")));
                        data.setEaQty((int)Double.parseDouble(AselTran.GetValue(rs, "item_qty")));
                        list.add(data);

                    }
                    ocheck = "kek";
                    list_amount = _result.getPropertyCount();
                    Log.wtf("list", String.valueOf(list_amount));
                    adapter = new ListViewAdapterDeliverItem(this, list);
                    m_oListView.setAdapter(adapter);

                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                break;
            }

            case "usp_COM_GetSlipNo" : {
                try {
                    for (int i = 1; i < _result.getPropertyCount(); i++) {
                        SoapObject rs = (SoapObject) _result.getProperty(i);
                        slipNo = AselTran.GetValue(rs, "slip_no");
                    }

                    //write code for saving the signature here
                    Toast.makeText(PopupDeliver.this, "수취 확인하였습니다.", Toast.LENGTH_SHORT).show();
                    signImage = signaturePad.getSignatureBitmap();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    signImage.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();
                    Log.wtf("bytesize", String.valueOf(b.length));
                    String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                    SaveAndDialog(encodedImage);


                    // new SOAPLoadTask(this, this).execute("usp_MOB_DeliveryFix_img", SOAPLoadTask.convertParams(_param3));

                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                break;
            }
        }


    }


    @Override
    public void onError(SoapObject _result) {
        Toast.makeText(this, "통신에 실패하였습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

            }
        }
    }



    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //바깥레이어 클릭시 안닫히게
        if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
            return false;
        }
        return true;
    }


}